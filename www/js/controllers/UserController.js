/**
 * Created by HP on 12/29/2016.
 */
(function () {
  'use strict';

  angular
    .module('kl-ionic-app.controllers')
    .controller('UserCtrl', UserCtrl);

  UserCtrl.$inject = ['$http', '$log', 'UserService', 'LoaderService'];

  /* @ngInject */
  function UserCtrl($http, $log, UserService, LoaderService) {
    var vm = this;
    vm.userLiked = [];
    vm.userFollowed = [];

    activate();

    //on click 'Like' button, the user adds into the list of liked users
    vm.doActionLike = function (key, name) {
      LoaderService.notifyLoader('You Liked ' + name + "!", 1000);
      vm.userLiked.push(key);
    }
    //on click 'Follow' button, the user adds into the list of followed users
    vm.doActionFollow = function (key, name) {
      LoaderService.notifyLoader('You are now following ' + name + "!", 1000);
      vm.userFollowed.push(key);
    }
    //it calls the function of UserService to fetch the list of users
    function activate() {
      var userCount = 50; //number of users to be fetched
      LoaderService.showLoader();
      UserService.getUserList(userCount).then(function (response) {
        LoaderService.hideLoader();
        vm.userList = response;
      }, function (err) {
        LoaderService.hideLoader();
        $log.log(err);
      });
    }
  }

})();

