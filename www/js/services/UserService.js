/**
 * Created by HP on 12/29/2016.
 */
(function () {
  'use strict';

  angular
    .module('kl-ionic-app.services')
    .service('UserService', UserService);

  UserService.$inject = ['$q', '$http', '$log'];

  /* @ngInject */
  function UserService($q, $http, $log) {
    var service = {};
    var apiUrl = 'https://randomuser.me/api/?results=';
    service.getUserList = getUserList;
    return service;

    function getUserList(userCount) {
      return $q(function (resolve, reject) {
        $http.get(apiUrl + userCount).then(function (response) {
          if (response && response.data && response.data.results) {
            resolve(response.data.results);
          }
          else {
            reject({success: false});
          }

        }, function (err) {
          $log.log(err);
          reject(err);
        });
      });
    }
  }

})();

