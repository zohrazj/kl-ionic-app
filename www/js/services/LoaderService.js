/**
 * Created by HP on 12/29/2016.
 */
(function () {

  "use strict";

  angular.module('kl-ionic-app.services').factory('LoaderService', LoaderService);

  LoaderService.$inject = ['$q', '$http', '$log', '$timeout', '$ionicLoading'];

  function LoaderService($q, $http, $log, $timeout, $ionicLoading) {
    var service = {};
    service.showLoader = showLoader;
    service.hideLoader = hideLoader;
    service.notifyLoader = notifyLoader;
    return service;

    function showLoader() {
      return $q(function (resolve, reject) {
        var response;
        $ionicLoading.show({}).then(function () {
          resolve();
        });
      });
    }

    function hideLoader() {
      return $q(function (resolve, reject) {
        var response;
        $ionicLoading.hide().then(function () {
          $log.log("The loading indicator is now hidden");
        });
        resolve();
      });
    }


    function notifyLoader(template, duration) {
      duration = duration ? duration : 1500;
      return $q(function (resolve, reject) {
        var response;
        $ionicLoading.show({
          template: template,
          duration: duration
        }).then(function () {
          $log.log("The loading indicator is now notified");
        });

      });
    }
  }

})();
